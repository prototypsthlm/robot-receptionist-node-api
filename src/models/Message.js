const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const MessageSchema = new Schema({
  user: { type: String, required: true },
  body: { type: String, required: true },
  recipient: { type: String, required: true }
});

mongoose.model('Message', MessageSchema);

module.exports = mongoose.model('Message');
