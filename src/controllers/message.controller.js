var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var Message = require('../models/Message');

router.use(
  bodyParser.urlencoded({
    extended: true
  })
);
router.use(bodyParser.json());

router.get('/:recipient', (req, res) => {
  Message.findOne({ recipient: req.params.recipient }, (error, message) => {
    error || !message
      ? res.status(500).send({
          error:
            'An error occurred while trying to find message. Maybe the message doesnt exist. Status code 500: Internal server error'
        })
      : Message.findByIdAndRemove(message._id, (error, messageDeleted) => {
          error
            ? res.status(500).send({
                error:
                  'An error occurred while trying to find message. Maybe the message doesnt exist. Status code 500: Internal server error'
              })
            : res.status(200).send(message);
        });
  });
});

router.post('/', (req, res) => {
  Message.create(req.body, (error, message) => {
    error
      ? res.status(500).send({
          error: 'An error have occurred. Please try again later'
        })
      : res.status(200).send({
          success: 'Message successfully posted'
        });
  });
});

module.exports = router;
