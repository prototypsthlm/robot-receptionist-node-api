# Robot Receptionist Messaging Api

## Deployment with Now

Install the now-cli

```bash
$ npm i -g now
```

Verify that Now-cli has been installed. Then from the project root, run

```bash
$ now
```

You might be prompted to create an account if you don't already have one.

After the deployment is complete, the api should be available at https://roborep.now.sh

_IMPORTANT_
If it's not available at the above url, you will have to alias it manually like so:

```bash
$ now alias https://<url-received-from-deploy>.now.sh roborep
```

## Endpoints

These are the endpoints available

```
/messages/:recipient
```

_GET_
Retrieves a posted message based on parameter recipient (String)

```
/messages/
```

_POST_
Posts a new message. Expects a json object as body.

```js
{
  "user": "Bjorn",
  "recipient": "Linda",
  "body": "I have a fish in my backpack. Let's eat it together!"
}
```
