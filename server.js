import http from 'http';
require('dotenv').config();

import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import morgan from 'morgan';
import mongoose from 'mongoose';

const app = express();
const server = http.Server(app);

/* Database */
mongoose.connect(process.env.DBCONNECTION);

/* Cors */
app.use(
  cors({
    origin: process.env.ALLOW_ORIGIN,
    credentials: true,
    allowedHeaders: 'X-Requested-With, Content-Type, Authorization',
    methods: 'GET, POST, PATCH, PUT, DELETE, OPTIONS'
  })
);

/* Logging */
app.use(morgan('dev'));

/* Parsing */
app.use(bodyParser.urlencoded({ extended: false }));

/* Controllers */
const MessageController = require('./src/controllers/message.controller');
app.use('/messages', MessageController);

/* Start */
const port = process.env.PORT || 7770;

server.listen(port, () =>
  console.log('Roborep api running at http://localhost:' + port)
);
